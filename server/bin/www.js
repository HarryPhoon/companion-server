#!/usr/bin/env node

const app = require('../app');
const db = require('debug');
const debug = require('debug')('server');
const http = require('http');
const WebSocket = require('ws');
const jwt = require('jsonwebtoken');
const url = require('url');

const auth = require('../utils/auth.js');
const database = require('../utils/database.js');
const utils = require('../utils/utilities.js');
const tokens = require('../utils/tokens.js');

auth.setup();

/**
 * Get port from environment and store in Express.
 */

let port = normalizePort(process.env.PORT || '3001');
app.set('port', port);

/**
 * Create HTTP server.
 */

let server = http.createServer(app);

/**
 * Configure WebSocket server
 */

let wss = new WebSocket.Server({
  'noServer': true,
  'path': "/api/ws",
  'clientTracking': true,
});
 
wss.getUniqueID = function() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return '#' + s4();
};

wss.sendMessage = function(message) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(message));
    }
  });
}

wss.handleConnection = function(request, ws) {
  const parameters = url.parse(request.url, true);
  const token = jwt.decode(parameters.query.token);

  console.debug(token);

  ws.id = token.sub + wss.getUniqueID();
  ws.chatRoom = token.perm;

  console.log(`${ws.id} has just connected.`);
}

wss.showUsers = function() {
  const clients = [];
  wss.clients.forEach(function each(client) {
    clients.push(client.id);
  });
  console.debug(clients);
}

wss.handleVerses = function(verse) {
  let verses = [];
  for (let i = 0; i < verse; i++) {
    verses[i] = i + 1;
  }
  return verses;
}

wss.sendActualData = async function() {
  let message = {};

  try {
    const query = "SELECT * FROM meta WHERE id = 1";
    let meta = await database.get(query, []);

    switch(meta.type) {
      case "song":
        let verse = await utils.toArray(meta.verse);

        let activeverse = meta.activeverse == null ? [] : await utils.toArray(meta.activeverse);

        verse = await utils.compareArrays(verse, activeverse);

        message = {
          type: meta.type,
          number: meta.number,
          verse: verse,
          activeverse: activeverse,
        };
        break;
      case "psalm":
        let query = "SELECT id, text FROM psalms WHERE id = ?";
        let psalm = await database.get(query, [meta.number]);

        message = {
          type: meta.type,
          number: psalm.id,
          psalmtext: psalm.text
        };
        break;
      default:
        message = {
          type: "blank",
          number: "",
          verse: [],
          activeverse: []
        };
        break;
    }
  }
  catch (e) {
    console.error(e);
  }
  finally {
    console.debug(message);
    wss.sendMessage(message);
  }
}
 
wss.on('connection', function Connection(ws, req) {
  wss.handleConnection(req, ws);
  
  wss.showUsers();

  wss.sendActualData();

  ws.on('message', async function Message(data) {
    data = JSON.parse(data);

    switch(data.type) {
      case "song":
        try {
          let query = "SELECT id, verse FROM songs WHERE id = ?";
          let song = await database.get(query, [data.number]);

          if (!song) {
            song = {
              id: data.number,
              type: "song",
              verse: 0
            };
          }
          song.verse = await wss.handleVerses(song.verse);


          if (song) {
            query = "UPDATE meta SET (number, type, verse, activeverse) = (?,?,?,?) WHERE id = 1";
            let updated = await database.run(query, [`${song.id}`, `${data.type}`, `${song.verse}`, ""]);
            if (!updated) {
              console.error(`Failed to update DB using query ${query}`);
              return;
            } 
          }

          query = "SELECT number, type, verse FROM meta WHERE id = 1";
          let meta = await database.get(query);

          if (song && meta) {
            let message = {
              number: meta.number,
              verse: utils.verseToArray(meta.verse),
              activeverse: [],
              type: meta.type
            };
            console.debug(message);
            wss.sendMessage(message);
          }
        }
        catch (e) {
          console.error(e);
        }
        break;
      case "psalm":
        try {
          let query = "SELECT id, text FROM psalms WHERE id = ?";
          let psalm = await database.get(query, [data.number]);

          if (!psalm) {
            throw "Psalm doesn't exist";
          }

          if (psalm) {
            query = "UPDATE meta SET (number, type, verse, activeverse) = (?,?,?,?) WHERE id = 1";
            let updated = await database.run(query, [`${psalm.id}`, `${data.type}`, "", ""])
            if (!updated) {
              console.error(`Failed to update DB using query ${query}`);
              return;
            } 
          }

          query = "SELECT number, type FROM meta WHERE id = 1";
          let meta = await database.get(query);

          if (psalm && meta) {
            let message = {
              type: meta.type,
              number: psalm.id,
              psalmtext: psalm.text
            };
            console.debug(message);
            wss.sendMessage(message);
          }
        }
        catch (e) {
          console.error(e);
        }
        break;
      case "addverse":
        try {
          let query = "SELECT verse, activeverse FROM meta WHERE id = 1";
          let meta = await database.get(query);

          let oldVerse = await utils.toArray(meta.verse);
          let oldActiveverse = await utils.toArray(meta.activeverse);

          let newActiveverse = oldActiveverse == [] ? await utils.toArray(data.verse) : oldActiveverse.concat(parseInt(data.verse)).sort(function(a, b){return a-b})
          let newVerse = await utils.compareArrays(oldVerse, newActiveverse);

          query = "UPDATE meta SET (activeverse) = (?) WHERE id = 1";
          let changed = await database.run(query, [`${newActiveverse}`]);

          if (meta && changed) {
            let message = {
              type: data.type,
              verse: newVerse,
              activeverse: newActiveverse
            };
            console.debug(message);
            wss.sendMessage(message);
          }
        } catch (e) {
          console.error(e);
        }
        break;
      case "delverse":
        try {
          let query = "SELECT verse, activeverse FROM meta WHERE id = 1";
          let meta = await database.get(query);

          let oldVerse = await utils.toArray(meta.verse);
          let oldActiveverse = await utils.toArray(meta.activeverse);

          let toDelete = await oldActiveverse.indexOf(parseInt(data.verse));

          oldActiveverse.splice(toDelete, 1);

          let newActiveverse = oldActiveverse;
          let newVerse = await utils.compareArrays(oldVerse, oldActiveverse);

          query = "UPDATE meta SET (activeverse) = (?) WHERE id = 1";
          let updated = await database.run(query, [`${newActiveverse}`]);

          if (meta && updated) {
            let message = {
              type: data.type,
              verse: newVerse,
              activeverse: newActiveverse
            };
            console.debug(message);
            wss.sendMessage(message);
          }
        } catch (e) {
          console.error(e);
        }
        break;
      case "blank":
        try {
          query = "UPDATE meta SET (number, type, verse) = (?,?,?) WHERE id = 1";
          let updated = await database.run(query, [null, null, null]);

          if (updated) {
            let message = {
              number: "",
              verse: "",
              activeverse: [],
              type: "blank"
            };
            wss.sendMessage(message);
          }
        }
        catch (e) {
          console.error(e);
        }
        break;
      case "refresh":
        wss.sendActualData();
      default:
        console.log("Received unknown message format.");
        break;
    }
  });

  ws.onclose = function onClose(e) {
    console.log("Socket has been closed.");
  };

  ws.onerror = function onError(err) {
    console.error("Socket encountered error: ", err.message, " Closing socket.");
    ws.close();
  };
})

server.on('upgrade', async function upgrade(req, socket, head) {
  const onSocketError = (err) => {
    console.error(`Websocket socket error: ${err}`);
  }

  const unauthorized = (err) => {
    console.error(`Websocket connection rejected: ${err}`);
    socket.write("HTTP/1.1 401 Unauthorized\r\n\r\n");
    socket.destroy();
  }

  socket.on('error', onSocketError);

  const query = url.parse(req.url, true).query;

  let isTokenValid = false;
  try {
    isTokenValid = await tokens.verifyAccessToken(query.token);
  } catch (err) {
    return unauthorized(err);
  }

  if (!isTokenValid)
   return unauthorized("Invalid token");

  socket.removeListener('error', onSocketError);

  wss.handleUpgrade(req, socket, head, function done(ws) {
    wss.emit('connection', ws, req, head);
  });
});

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
