const jwt = require('jsonwebtoken');
const { v1: uuidv1 } = require('uuid');
const database = require('./database');
const config = require('../config/config');
const debug = require('debug')('tokens');

const tokenConfig = {
  accessToken: {
    expiration: '5m',
  },
  refreshToken: {
    expiration: '5184000000',
  },
}

const tokens = {
  generateAccessToken(payload) {
    return new Promise((resolve, reject) => {
      try {
        if (!payload)
          throw "No payload provided";
        
        // Generate tolet accessToken, accessClaims;kens
        const accessClaims = {
          'sub': payload.username,
          'perm': payload.permission
        }
          
        const accessToken = jwt.sign(accessClaims, config.token.secret, {
          expiresIn: payload.expiration ? payload.expiration : tokenConfig.accessToken.expiration
        })
        
        resolve(accessToken);
      } catch (err) {
        reject(err);
      }
    })
  },

 generateRefreshToken(payload) {
    const refreshToken = {
      token: uuidv1(),
      username: payload.username,
      iss: Date.now()
    }

    return(refreshToken);
  },

  setRefreshToken(newToken, oldToken = "") {
    return new Promise(async (resolve, reject) => {
      try {
        if (!newToken) {
          reject("No token provided");
        }

        let oldTokenExists = false;
        
        if (oldToken.length !== 0) {
          oldTokenExists = await database.selectOne("token", "tokens", "token", oldToken);
        }
        
        if (oldTokenExists) {
          const sql = `UPDATE tokens SET (token, iss) = (?,?) WHERE token = ? AND username = ?`;
          const info = await database.run(sql, [`${newToken.token}`, `${newToken.iss}`, `${oldToken}`, `${newToken.username}`]);

          debug(info);

          if (info.changes > 0) {
            resolve(true);
          } else {
            throw "No changes during token update";
          }
        } else {
          const tokenInserted = await database.insertMore("tokens", "token, username, iss", [newToken.token, newToken.username, newToken.iss]);
          
          if (tokenInserted) {
            resolve(true);
          } else {  
            throw "No token was inserted";
          }
        }
      } catch(err) {
        reject(err);
      }
    })
  },

  verifyAccessToken(token) {
    return new Promise((resolve, reject) => { 
      try {
        jwt.verify(token, config.token.secret, function(err, token) {
          if (err) {    
            throw "Invalid active token.";
          }
          
          if (token) {
            resolve(token);
          }
          else {
            throw "Invalid access token";
          }
        })
      }
      catch (err) {
        reject(err);
      }
    })
  },

  verifyRefreshToken(token) {
    return new Promise(async (resolve, reject) => {
      try {
        const sql = "SELECT token FROM tokens WHERE token = ?";

        const refreshToken = await database.get(sql, [token]);
        
        if (!refreshToken) {
          throw "False refresh token (not found in database)";
        }
        
        const currentTime = Date.now();
        const refreshTokenTime = refreshToken.iss;

        if (currentTime - refreshTokenTime > tokenConfig.refreshToken.expiration) {
          database.deleteOne();
          throw "The refresh token has expired";
          // Should I throw an error or resolve false??
        }
        else {
          resolve(true);
        }
      }
      catch (err) {
        reject(err);
      }
    })
  },

  refresh(oldRefreshToken, payload) {
    console.debug("utils/tokens: refresh");
    return new Promise(async (resolve, reject) => {
      try {
        // Check if refreshToken is whitelisted
        const sql = `SELECT token FROM tokens WHERE token = ?`;
        const oldRefreshTokenExists = database.get(sql, [oldRefreshToken]);

        if (!oldRefreshTokenExists) {
          throw "Refresh token is not whitelisted (not found in database)";
        }

        // Generate a new active and refresh tokens
        const newAccessToken = await this.generateAccessToken(payload);
        const newRefreshToken = await this.generateRefreshToken(payload);

        if (!newAccessToken || !newRefreshToken) {
          reject("Error during token generation.");
        }

        // Update refresh token in the database
        const updatedToken = await this.setRefreshToken(newRefreshToken, oldRefreshToken);

        newTokens = {
          accessToken: newAccessToken,
          refreshToken: newRefreshToken.token
        };

        if (updatedToken)
          resolve(newTokens);
      }
      catch (err) {
        reject(err);
      }
    })
  }
}

module.exports = tokens;
