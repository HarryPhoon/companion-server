import 'whatwg-fetch';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';

import EasyLoginForm from '../components/EasyLoginForm';
import { useAuth } from '../services/AuthService';

function LoginPage() {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState({
    error: false,
    requireLogin: false,
    wrongCredentials: false,
  });
  const { login, quicklogin, refresh } = useAuth();
  const navigate = useNavigate();

  const switchLogin = () => {
    setStatus({
      ...status,
      requireLogin: !status.requireLogin
    });
  }

  const handleFormChange = (event) => {
    event.preventDefault();

    setPassword(event.target.value);

    if (status.wrongCredentials) {
      setStatus({
        ...status,
        wrongCredentials: false,
      });
    }
  };

  const onClickLogin = async (event) => {
    event.preventDefault();

    if (event.currentTarget.id === "back") {
      return switchLogin();
    }

    if (event.currentTarget.id === "general") {
      const username = event.currentTarget.id;
      let ok = false;

      setUsername(username);

      const newTokens = await refresh();

      if (newTokens)
        ok = await quicklogin(username, newTokens.refreshToken);

      if (ok)
        return navigate("/");
      else
        return switchLogin();
    }
  };

  // send to API and handle client authentication
  const processForm = async (event) => {
    event.preventDefault();

    const err = await login({ username: username, password: password });
    if (!err) {
      setStatus({
        ...status,
        error: false,
        wrongCredentials: false,
      });
      navigate("/");
    } else if (err.wrongCredentials) {
      setStatus({
        ...status,
        error: false,
        wrongCredentials: true,
      });
    } else if (err.error) {
      setStatus({
        ...status,
        error: true,
        wrongCredentials: false,
      });
    }
  }

  return (
    <div className='page-parent'>
      <Grid container justifyContent='center' alignItems='center' direction='column' className='container-full'>
        <Grid item>
          <EasyLoginForm onSubmit={processForm} onChange={handleFormChange} onClick={onClickLogin} status={status} password={password} />
        </Grid>
      </Grid>
    </div>
  )
}

export default LoginPage;
